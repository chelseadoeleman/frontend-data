# Proces 


## Week 1


### Day 1 | 12 October 2018

First we had another kick off, with a presentation about D3. The following weeks we will build another graph with D3, but this time interaction will be added. We could use our other graph and build on it, but I wanted to tackle some new data that could lead to some new interesting insights. 

After that we had another presentation by Emma, who worked on a datavisualisation with R. Unfortunately she didn't dive into the code, to see what variables she used to visualise her data. But she did do some explaining about her research and user tests. This was a lot of information to take in and when I say a lot I mean a lot... lot..., especially on monday morning. The things I did manage to catch were very useful, for our own data visualisation. But for me it felt like an explosion of information, where I wasn't sure about the things that were revelant for our own data visualisation.

The rest of the day I spent browsing through the data again and starting from scratch with the Api. From there I want to build my data like last time, so it can be loaded dynamically and also will be in the right format for D3. 

### Day 2 | 13 October 2018

Today I started with a general idea about the data I wanted to use. First I wanted to compare the number of pages with the number of cover images through the years. After that I started sketching and decided what I wanted my interaction to look like and how it would work. In the end I threw this idea out the window and started all over again. But this time with movies. To compare the total minutes of movie genres throughout the years. And to zoom in on a certain genre, to see wich movies might have a longer character title to the duration of that movie. Where the data could also be viewed in certain periods. Then a lot of sketching happened again. 

In the meantime we also had een standup, wich was very insightful into what everyone else was going to visualise and the different variables they were going to use for that. 

My goal for today was to get all the data out of the Api. However duration was enveloped in some other data, thus I had a hard time trying to get the specific data I wanted to use. By the end of the day I unfortunately hadn't completed this, but will resume first thing in the moring. Well after another presenation that is... 



### Day 3 | 14 October 2018



### Day 4 | 15 November 2018



### Day 5 | 16 November 2018




## Week 2


### Day 6 | 19 November 2018



### Day 7 | 20 November 2018



### Day 9 | 22 November 2018



### Day 10 | 23 November 2018

Assessment day!